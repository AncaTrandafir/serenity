package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class FinalCollectionPage extends PageObject {

    public List<String> getText() {
        WebElementFacade bodyList = find(By.className("filter-bar"));
        return bodyList.findElements(By.tagName("h1")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }

    @FindBy(css = "select[class = \'filter-sorting-value-list\']")
    private WebElementFacade dropDownBox;

    public void click_dropDown() { dropDownBox.click(); }

    public void selectDropdown(String sort) {
        dropDownBox.selectByValue(sort);
    }

}