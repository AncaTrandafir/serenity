package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.decathlon.ro/")
public class MainPage extends PageObject {

    // Bara de cautare
    public void enterReference_PressEnter(String reference) {
        searchReference.type(reference);
        searchReference.sendKeys(Keys.ENTER);
    }
    @FindBy(id="header-searchbar")
    private WebElementFacade searchReference;

    // Butonul de login
//    @FindBy(className="accountIcon")
    @FindBy(id="new-header-welcome-content")
    private WebElementFacade loginButton;

    public void login_click() {
        loginButton.click();
    }

    // Butonul de Final colectie, pentru sortare dupa pret crescator
    @FindBy(xpath = "//*[@id=\"nav\"]/div[1]/ul/li[7]")
    private WebElementFacade finalCollectionButton;

    public void finalCollection_click() {
        finalCollectionButton.click();
    }

    // Butonul Schimba locatia
    @FindBy(css = "span[class = \'icon-text\']")
    private WebElementFacade changeLocationButton;

    public void changeLocation_click() {
        changeLocationButton.click();
    }
}