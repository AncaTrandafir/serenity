package ExamenTestare.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class ForgotPassPage extends PageObject {

    public List<String> getText() {
        WebElementFacade bodyList = find(By.className("mydkt-box-title"));
        return bodyList.findElements(By.tagName("label")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}