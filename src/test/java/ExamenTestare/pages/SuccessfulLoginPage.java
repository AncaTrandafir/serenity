package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class SuccessfulLoginPage extends PageObject {

    public List<String> getText() {
        WebElementFacade bodyList = find(By.id("user-info"));
        return bodyList.findElements(By.tagName("div")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}