package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class NewAccountPage extends PageObject {

  //  @FindBy (css = "input[type = 'email', id = \'mydkt-Email\']")
    @FindBy (xpath = "//*[@id=\"mydkt-Email\"]")
    private WebElementFacade emailTextBox;

    @FindBy(css = "input[id = \'mydkt-ConfEmail\']")
    private WebElementFacade confirmEmailTextBox;

    @FindBy (css = "button[id = \'creationCompte1.Valid\']")
    private WebElementFacade validationButton;

    public void enter_email(String email) {
        emailTextBox.type(email);
    }

    public void confirm_email(String pass) {
        confirmEmailTextBox.type(pass);
    }

    public void connect() {
        validationButton.click();
    }


}