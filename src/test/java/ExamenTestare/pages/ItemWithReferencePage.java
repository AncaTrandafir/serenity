package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.stream.Collectors;

public class ItemWithReferencePage extends PageObject {

    public List<String> getReference() {
        WebElementFacade referenceList = find(By.tagName("h1"));
        return referenceList.findElements(By.tagName("span") ).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }

}