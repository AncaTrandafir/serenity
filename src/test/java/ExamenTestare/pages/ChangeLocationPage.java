package ExamenTestare.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class ChangeLocationPage extends PageObject {

    @FindBy(css = "input[id = \'decathlon-woosmap-search-text\']")
    private WebElementFacade inputCity;

    public void enterCity(String city) { inputCity.type(city); }

    @FindBy(css = "button[id = \'woosmap-search-button\']")
    private WebElementFacade searchButton;

    public void search() {
        searchButton.click();
    }

    public List<String> getLocation() {
        WebElementFacade bodyList = find(By.id("decathlon-woosmap-search-container"));
        return bodyList.findElements(By.tagName("div")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }

}