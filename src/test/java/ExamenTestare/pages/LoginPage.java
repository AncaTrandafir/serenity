package ExamenTestare.pages;

import com.google.inject.internal.cglib.core.$ClassNameReader;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

import java.util.List;
import java.util.stream.Collectors;

public class LoginPage extends PageObject {
//  Login

   // @FindBy(xpath="//*[@id=\"defaut.Email\"]")
    @FindBy (css = "input[class = \'mydkt-textbox\']")
    private WebElementFacade emailTextBox;

    @FindBy(css = "input[type = \'password\']")
    private WebElementFacade passwordTextBox;

    @FindBy(css = "button[id = \'defaut.Connecter\']")
    private WebElementFacade connectButton;

    public void enter_email(String email) {
        emailTextBox.type(email);
    }

    public void enter_password(String pass) {
        passwordTextBox.type(pass);
    }

    public void connect() {
        connectButton.click();
    }

    // New Account
//   @FindBy (css = "button[type = 'button', id = \'defaut.Carte\']")
    @FindBy (xpath = "//*[@id=\"defaut.Carte\"]")
    private WebElementFacade newAccountButton;

    public void newAccount_click() {
        newAccountButton.click();
    }
}