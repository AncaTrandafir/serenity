//package ExamenTestare.features.search;
//
//import ExamenTestare.pages.LoginPage;
//import ExamenTestare.steps.serenity.ItemWithReferenceSteps;
//import ExamenTestare.steps.serenity.LoginSteps;
//import ExamenTestare.steps.serenity.MainPageSteps;
//import ExamenTestare.steps.serenity.SuccessfulLoginSteps;
//import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
//import net.serenitybdd.junit.runners.SerenityRunner;
//import net.thucydides.core.annotations.Issue;
//import net.thucydides.core.annotations.Managed;
//import net.thucydides.core.annotations.Steps;
//import net.thucydides.junit.annotations.UseTestDataFrom;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.openqa.selenium.WebDriver;
//
////aici avem teste parametrizate
//@RunWith(SerenityParameterizedRunner.class)
////fisierul cu datele de intrare este cel precizat pe randul urmator
//@UseTestDataFrom("src\\test\\resources\\LoginLogout.csv")
//
//public class LoginLogoutParametrised {
//
//    @Managed(uniqueSession = true)
//    public WebDriver webdriver;
//
//    @Steps
//    public MainPageSteps mainPageSteps;
//    @Steps
//    public LoginSteps loginSteps;
//    @Steps
//    public SuccessfulLoginSteps successfulLoginSteps;
//
//    String email, password;
//
//    @Issue("#LoginLogoutSuccessful-1")
//    @Test
//    public void ValidLoginTest() {
//        //pornim de la pagina de login
//        mainPageSteps.is_the_home_page();
//        mainPageSteps.login();
//        //se introduc toate datele (cate un rand citit din fisierul de intrare) si se da click pe butonul de Login
//        loginSteps.enter_email_password_connect(email, password);
//        //verificam ca am ajuns pe pagina de raportare a erorii
//        successfulLoginSteps.shouldSeeBunVenit("Bun venit");
//    }
//
//    @Issue("#errorLogin-1")
//    @Test
//    public void NonValidLogintest() {
//        mainPageSteps.is_the_home_page();
//        mainPageSteps.login();
//        loginSteps.enter_email_password_connect(email, password);
//        //verificam ca am ajuns pe pagina de raportare a erorii
//        successfulLoginSteps.shouldSeeBunVenit("Parola sau adresa de e-mail sunt greşite");
//    }
//
//}
