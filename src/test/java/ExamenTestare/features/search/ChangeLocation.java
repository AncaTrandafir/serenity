package ExamenTestare.features.search;

import ExamenTestare.steps.serenity.ChangeLocationSteps;
import ExamenTestare.steps.serenity.ItemWithReferenceSteps;
import ExamenTestare.steps.serenity.LocationChangedSuccessfullySteps;
import ExamenTestare.steps.serenity.MainPageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class ChangeLocation {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public MainPageSteps mainPageSteps;
    @Steps
    public ChangeLocationSteps changeLocationSteps;
    @Steps
    public LocationChangedSuccessfullySteps locationChangedSuccessfullySteps;

    @Issue("#ChangeLocation-1")

    @Test
    public void changeLocation() {
        mainPageSteps.is_the_home_page();
        mainPageSteps.changeLocation();
        changeLocationSteps.changeLocation("Constanta");
        changeLocationSteps.should_see_text("Am găsit 1 magazin");
    }

}
