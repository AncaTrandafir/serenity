//package ExamenTestare.features.search;
//
//import ExamenTestare.steps.serenity.LoginSteps;
//import ExamenTestare.steps.serenity.MainPageSteps;
//import ExamenTestare.steps.serenity.SuccessfulLoginSteps;
//import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
//import net.serenitybdd.junit.runners.SerenityRunner;
//import net.thucydides.core.annotations.Issue;
//import net.thucydides.core.annotations.Managed;
//import net.thucydides.core.annotations.Steps;
//import net.thucydides.junit.annotations.UseTestDataFrom;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.openqa.selenium.WebDriver;
//
//@RunWith(SerenityRunner.class)
//public class LoginLogout {
//
//    @Managed(uniqueSession = true)
//    public WebDriver webdriver;
//
//    @Steps
//    public MainPageSteps mainPageSteps;
//    @Steps
//    public LoginSteps loginSteps;
//    @Steps
//    public SuccessfulLoginSteps successfulLoginSteps;
//
//    @Issue("#LoginLogoutSuccessful-1")
//    @Test
//    public void ValidLoginTest() {
//        //pornim de la pagina de login
//        mainPageSteps.is_the_home_page();
//        mainPageSteps.login();
//        //se introduc toate datele (cate un rand citit din fisierul de intrare) si se da click pe butonul de Login
//        loginSteps.enter_email_password_connect("anca.trandafir@ymail.com", "Hennazara51");
////        loginSteps.enter_email("anca.trandafir@ymail.com");
////        loginSteps.enter_password("Hennazara51");
////        loginSteps.connect();
//        //verificam ca am ajuns pe pagina de raportare a erorii
//        successfulLoginSteps.shouldSeeBunVenit("Bun venit");
//    }
//
//    @Issue("#errorLogin-1")
//    @Test
//    public void NonValidLogintest() {
//        mainPageSteps.is_the_home_page();
//        mainPageSteps.login();
////        loginSteps.enter_email("anca.trandafir@ymail.com");
////        loginSteps.enter_password("Hennazara");
////        loginSteps.connect();
//       loginSteps.enter_email_password_connect("anca.trandafir@ymail.com", "hennazara");
//        //verificam ca am ajuns pe pagina de raportare a erorii
//        successfulLoginSteps.shouldSeeBunVenit("Parola sau adresa de e-mail sunt greşite");
//    }
//
//}
