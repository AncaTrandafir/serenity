package ExamenTestare.features.search;

import ExamenTestare.steps.serenity.FinalCollectionSteps;
import ExamenTestare.steps.serenity.ItemWithReferenceSteps;
import ExamenTestare.steps.serenity.MainPageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class AscendingSort {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public MainPageSteps mainPageSteps;
    @Steps
    public FinalCollectionSteps finalCollectionSteps;

    @Issue("#AscendingOrder-1")
    @Test
    public void AscendingSortTest() {
        mainPageSteps.is_the_home_page();
        mainPageSteps.finalCollection();
        finalCollectionSteps.should_see_text("FINAL DE COLECTIE");
        finalCollectionSteps.ascendingSort("/C-177431-final-de-colectie/T-478324_478354");
        }

}
