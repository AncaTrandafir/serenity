package ExamenTestare.features.search;

import ExamenTestare.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class NewAccNewPass {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public MainPageSteps mainPageSteps;
    @Steps
    public LoginSteps loginSteps;
    @Steps
    public NewAccountSteps newAccountSteps;
    @Steps
    public ForgotPassSteps forgotPassSteps;


    @Issue("#NewAccount-1")
    @Test
    public void NewAccount_NewPassword() {
        mainPageSteps.is_the_home_page();
        mainPageSteps.login();
        loginSteps.newAccountConnect();
        newAccountSteps.enterEmail("anca.trandafir@ymail.com", "anca.trandafir@ymail.com");
        forgotPassSteps.shouldSeeText("AŢI UITAT PAROLA ?");
    }

}
