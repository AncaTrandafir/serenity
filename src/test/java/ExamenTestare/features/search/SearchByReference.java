package ExamenTestare.features.search;

import ExamenTestare.pages.ItemWithReferencePage;
import ExamenTestare.steps.serenity.ItemWithReferenceSteps;
import ExamenTestare.steps.serenity.MainPageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

    @RunWith(SerenityRunner.class)
    public class SearchByReference {

        @Managed(uniqueSession = true)
        public WebDriver webdriver;

        @Steps
        public MainPageSteps mainPageSteps;
        @Steps
        public ItemWithReferenceSteps itemWithReferenceSteps;

        @Issue("#SearchByRerefence-1")

        @Test
        public void searchReference_seeItemWithReference() {
            mainPageSteps.is_the_home_page();
            mainPageSteps.enterReference("8378237");
            itemWithReferenceSteps.should_see_reference("CORT CU BEȚE ARPENAZ 4.1 4 PERSOANE 1 CAMERĂ QUECHUA");

        }

    }
