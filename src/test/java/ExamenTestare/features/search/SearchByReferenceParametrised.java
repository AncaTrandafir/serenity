package ExamenTestare.features.search;

import ExamenTestare.steps.serenity.ItemWithReferenceSteps;
import ExamenTestare.steps.serenity.MainPageSteps;
import ExamenTestare.steps.serenity.OopsSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src\\test\\resources\\FindReference.csv")
public class SearchByReferenceParametrised {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public MainPageSteps mainPageSteps;
    @Steps
    public ItemWithReferenceSteps itemWithReferenceSteps;
    @Steps
    public OopsSteps oopsSteps;

    String reference;

    @Issue("#SearchByRerefence-1")

//    @Test
//    public void searchReference_seeItemWithReference1() {
//        mainPageSteps.is_the_home_page();
//        mainPageSteps.enterReference(reference);
//        itemWithReferenceSteps.should_see_reference("CORT CAMPING 2 SECONDS");

//    }

    @Test
    public void searchReference_seeItemWithReference2() {
        mainPageSteps.is_the_home_page();
        mainPageSteps.enterReference(reference);
        itemWithReferenceSteps.should_see_reference("GHETE CĂLDUROASE MID");

    }



}
