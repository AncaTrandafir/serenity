package ExamenTestare.steps.serenity;

import ExamenTestare.pages.FinalCollectionPage;
import ExamenTestare.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class FinalCollectionSteps {

    FinalCollectionPage finalCollectionPage;

    @Step
    public void should_see_text(String text) {
        assertThat(finalCollectionPage.getText(), hasItem(containsString(text)));
    }

    @StepGroup
    public void ascendingSort(String sort) {
        finalCollectionPage.click_dropDown();
        finalCollectionPage.selectDropdown(sort);
    }

}