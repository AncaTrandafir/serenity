package ExamenTestare.steps.serenity;

import ExamenTestare.pages.LoginPage;
import ExamenTestare.pages.NewAccountPage;
import net.thucydides.core.annotations.StepGroup;

public class NewAccountSteps {

    NewAccountPage newAccountPage;

    @StepGroup
    public void enterEmail(String email, String confEmail) {
        newAccountPage.enter_email(email);
        newAccountPage.confirm_email(confEmail);
        newAccountPage.connect();
    }



}