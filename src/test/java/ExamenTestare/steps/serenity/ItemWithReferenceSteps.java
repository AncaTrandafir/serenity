package ExamenTestare.steps.serenity;

import ExamenTestare.features.search.SearchByReference;
import ExamenTestare.pages.ItemWithReferencePage;
import ExamenTestare.pages.MainPage;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class ItemWithReferenceSteps {

    ItemWithReferencePage itemWithReferencePage;

    // Verificam ca produsul afisat are codul dat ca referrinta
    @Step
    public void should_see_reference(String reference) {
        assertThat(itemWithReferencePage.getReference(), hasItem(containsString(reference)));
       //  assertEquals(itemWithReferencePage.getReference(), reference);
    }

}