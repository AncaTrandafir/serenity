package ExamenTestare.steps.serenity;

import ExamenTestare.pages.ChangeLocationPage;
import ExamenTestare.pages.FinalCollectionPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class ChangeLocationSteps {

    ChangeLocationPage changeLocationPage;

    @StepGroup
    public void changeLocation(String city) {
        changeLocationPage.enterCity(city);
        changeLocationPage.search();
    }

    @Step
    public void should_see_text(String text) {
        assertThat(changeLocationPage.getLocation(), hasItem(containsString(text)));
    }
}