package ExamenTestare.steps.serenity;

import ExamenTestare.pages.ItemWithReferencePage;
import ExamenTestare.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

public class LoginSteps {

    LoginPage loginPage;

    @StepGroup
    public void enter_email_password_connect(String email, String pass) {
        loginPage.enter_email(email);
        loginPage.enter_password(pass);
        loginPage.connect();
    }

//    @Step
//    public void enter_email(String email) {
//        loginPage.enter_email(email);
//    }
//
//    @Step
//    public void enter_password(String password) {
//        loginPage.enter_password(password);
//    }
//
//    @Step
//    public void connect() {
//        loginPage.connect();
//    }

    @Step
    public void newAccountConnect() {
        loginPage.newAccount_click();
    }
}