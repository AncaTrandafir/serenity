package ExamenTestare.steps.serenity;

import ExamenTestare.pages.MainPage;
import ExamenTestare.pages.SuccessfulLoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class SuccessfulLoginSteps {

    SuccessfulLoginPage successfulLoginPage;

    @Step
    public void shouldSeeBunVenit(String bunVenit) {
        assertThat(successfulLoginPage.getText(), hasItem(containsString(bunVenit)));
    }



}