package ExamenTestare.steps.serenity;

import ExamenTestare.pages.ChangeLocationPage;
import ExamenTestare.pages.LocationChangedSuccessfullyPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class LocationChangedSuccessfullySteps {

    LocationChangedSuccessfullyPage locationChangedSuccessfullyPage;

    @Step
    public void should_see_text(String text) {
        assertThat(locationChangedSuccessfullyPage.getLocation(), hasItem(containsString(text)));
    }
}