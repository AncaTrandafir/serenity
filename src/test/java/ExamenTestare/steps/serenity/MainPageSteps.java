package ExamenTestare.steps.serenity;

import ExamenTestare.pages.MainPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class MainPageSteps {

    MainPage mainPage;

    @Step
    public void is_the_home_page() {
        mainPage.open();
    }

    @Step
    public void enterReference(String reference) {
        mainPage.enterReference_PressEnter(reference);
    }

    @Step
    public void login() {
        mainPage.login_click();
    }


    @Step
    public void finalCollection() {
        mainPage.finalCollection_click();
    }

    @Step
    public void changeLocation() {
        mainPage.changeLocation_click();
    }

}