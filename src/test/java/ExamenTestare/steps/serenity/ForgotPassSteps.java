package ExamenTestare.steps.serenity;

import ExamenTestare.pages.ForgotPassPage;
import ExamenTestare.pages.SuccessfulLoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class ForgotPassSteps {

    ForgotPassPage forgotPassPage;

    @Step
    public void shouldSeeText(String text) {
        assertThat(forgotPassPage.getText(), hasItem(containsString(text)));
    }



}