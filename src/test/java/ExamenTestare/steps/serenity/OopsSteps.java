package ExamenTestare.steps.serenity;

import ExamenTestare.pages.OopsPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class OopsSteps {
    OopsPage oopsPage;

    @Step
    public void should_see_text(String text) {
        assertThat(oopsPage.get_textError(), hasItem(containsString(text)));
    }
}
